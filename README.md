# Atlassian Jira Software Server
This is a simple Jira Software Docker container to use as installation foundation for your own Jira Software installation with Docker.
 
Find out more about Jira at <https://www.atlassian.com/software/jira>

You can find the repository for this Dockerfile at <https://bitbucket.org/insanitydesign/docker-jira-software-server>

# Quick Start
For the directory in the environmental variable `JIRA_HOME` that is used to store Jira data (amongst other things) it is recommended to mount a host directory as a [data volume](https://docs.docker.com/userguide/dockervolumes/#mount-a-host-directory-as-a-data-volume):
 
Start Atlassian Jira Software Server:
 
    $> docker run -v /data/your-jira-home:/var/atlassian/application-data/jira --name="jira" -d -p 8005:8005 -p 8080:8080 insanitydesign/jira-software-server

**Success**. Jira is now available on [http://localhost:8080](http://localhost:8080)

Please ensure your container has the necessary resources allocated to it and check [Supported Platforms](https://confluence.atlassian.com/adminjiraserver071/supported-platforms-802592168.html) for further requirements.

## Configuration

### Memory / Heap Size
If you need to override Jira Software Server's default memory allocation, you can control the minimum heap (Xms) and maximum heap (Xmx) via the below environment variables.

* `JVM_MINIMUM_MEMORY`  
   The minimum heap size of the JVM
* `JVM_MAXIMUM_MEMORY`  
   The maximum heap size of the JVM

### Reverse Proxy Settings
If Jira is run behind a reverse proxy server, then you need to specify extra options to make Jira aware of the setup. They can be controlled via the below environment variables.

* `CATALINA_CONNECTOR_PROXYNAME` (default: NONE)  
   The reverse proxy's fully qualified hostname.
* `CATALINA_CONNECTOR_PROXYPORT` (default: NONE)  
   The reverse proxy's port number via which Jira is accessed.
* `CATALINA_CONNECTOR_SCHEME` (default: http)  
   The protocol via which Jira is accessed.
* `CATALINA_CONNECTOR_SECURE` (default: false)  
   Set 'true' if CATALINA_CONNECTOR_SCHEME is 'https'.
   
### JVM configuration
If you need to pass additional JVM arguments to Jira such as specifying a custom trust store, you can add them via the below environment variable

* `JVM_SUPPORT_RECOMMENDED_ARGS`  
   Additional JVM arguments for Jira
   
Example:

    $> docker run -e JVM_SUPPORT_RECOMMENDED_ARGS=-Djavax.net.ssl.trustStore=/var/atlassian/application-data/jira/cacerts -v jiraVolume:/var/atlassian/application-data/jira --name="jira" -d -p 8005:8005 -p 8080:8080 insanitydesign/jira-software-server

 
## Upgrade 
To upgrade to a more recent version of Jira Software Server you can simply stop the `Jira` container and start a new one based on a more recent image:
 
    $> docker stop jira
    $> docker rm jira
    $> docker run ... (see above)
 
As your data is stored in the data volume directory on the host, it will still be available after the upgrade.
 
_Note: Please make sure that you **don't** accidentally remove the `jira` container and its volumes using the `-v` option._
 
## Backup
Read more about data backups: [Backing up data](https://confluence.atlassian.com/adminjiraserver071/backing-up-data-802592964.html)
 
## Product Support
For Jira product support go to [support.atlassian.com](http://support.atlassian.com).

This image is build around OpenJDK which is not officially a supported Java platform for Jira. So far no issues have been found personally.

## Disclaimer
I am not related to Atlassian or Jira and do not explicitly endorse any relation. This source and the whole package comes without warranty. It may or may not harm your computer, server etc. Please use with care. Any damage cannot be related back to the author. The source has been tested on a virtual environment. Use at your own risk.

## Personal Note
I don't know if this is very useful for a lot of people but I required what is provided here. I hope this proves useful to you... with all its Bugs and Issues ;) If you like it you can give me a shout at [INsanityDesign](https://insanitydesign.com) or let me know via the repository.